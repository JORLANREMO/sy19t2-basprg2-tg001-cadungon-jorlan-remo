// Excercise 4 Pointer Dungeon Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

void printInventory(vector<Item*> items)
{
	cout << "INVENTORY: " << endl;
	cout << "=========" << endl;
	// Get the number of items in the vector
	int* numberOfItems = new int; 
	*numberOfItems = items.size();
	// Print the items
	for (int i = 0; i < *numberOfItems; i++)
	{
		cout << items[i]->name << " | " << items[i]->gold << endl;
	}
	cout << "=========" << endl;
	cout << endl;
}

Item* getRandomItem()
{
	int r = rand() % 5;
	Item* item = new Item; // Temporary item (while we create it)

	// Assign values based on random
	switch (r)
	{
	case 0:
		item->name = "Cursed Stone";
		item->gold = 0;
		break;
	case 1:
		item->name = "Mithril Ore";
		item->gold = 100;
		break;
	case 2:
		item->name = "Sharp Talon";
		item->gold = 50;
		break;
	case 3:
		item->name = "Thick Leather";
		item->gold = 25;
		break;
	case 4:
		item->name = "Jellopy";
		item->gold = 5;
		break;
	default:
		break;
	}
	return item;
}

// Returns all earned gold in 1 dungeon run
int enterDungeon(int* playerGold)
{
	bool willKeepLooting = true;

	// Array version
	vector<Item*> inventory;

	// We put the multiplier OUTSIDE the while loop
	// Because we want the value to persist between runs
	int multiplier = 1;
	int temporaryGold = 0;
	while (willKeepLooting) {
		// Get loot
		Item* loot = getRandomItem();
		inventory.push_back(loot);
		cout << "Current multiplier: " << multiplier << endl;
		cout << "Temporary gold: " << temporaryGold << endl;

		cout << "Looting... " << endl;
		system("pause");

		// Check if loot is cursed stone
		if (loot->name == "Cursed Stone")
		{
			cout << "Looted a Cursed Stone!" << endl;
			cout << "F" << endl;
			printInventory(inventory);
			// If cursed stone, end dungeon run.
			// No earned gold
			return 0;

		}
		

		// If not cursed stone
		else
		{
			// Multiply loot value by multiplier
			// Add lootValue to temporaryGold
			string lootName = loot->name;
			cout << "You got a " << lootName << "!" << endl;
			int itemValue = loot->gold * multiplier;
			cout << "Item value: " << itemValue << endl;
			temporaryGold += itemValue;
			loot->gold = itemValue;
			// Add loot to inventory
			inventory.push_back(loot);

			// Increment multiplier
			multiplier++;

			// Ask user if they want to exit or continue
			string input;
			cout << "Do you want to continue looting (y/n)?: ";
			cin >> input;
			if (input == "y")
			{
				willKeepLooting = true;
				system("cls");
			}
			else
			{
				willKeepLooting = false;
			}
		}

	
	}
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i]->name << endl;
	}

	for (int i = 0; i < inventory.size(); i++)
	{
		delete inventory[i];
	}

	// Exited out of the dungeon w/o dying.
	printInventory(inventory);

	// Return earned gold
	return temporaryGold;
}

int main()
{
	srand(time(NULL));

	int* playerGold = new int;
	*playerGold = 50;
	int dungeonFee = 25;

	while (*playerGold >= dungeonFee && *playerGold < 500)
	{
		cout << "Player total gold: " << *playerGold << endl;
		// Entering a dungeon costs a fee
		playerGold -= dungeonFee;

		cout << "Entering dungeon..." << endl;
		// Get the earned gold from the dungeon
		int earnedGold = enterDungeon(playerGold);
		// Add to playerGold
		playerGold += earnedGold;
	}


	system("pause");
	return 0;
}

