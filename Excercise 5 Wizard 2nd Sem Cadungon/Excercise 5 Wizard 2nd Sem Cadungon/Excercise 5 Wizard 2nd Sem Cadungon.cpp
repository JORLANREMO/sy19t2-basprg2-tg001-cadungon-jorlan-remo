// Excercise 5 Wizard 2nd Sem Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"
#include <iostream>
#include <string>
#include "Wizard.h"
#include "Spell.h"
#include <time.h>

using namespace std;


int main()
{
	Wizard* wizard = new Wizard("Warlock", 100, 10);
	Spell* fireball = new Spell(wizard, "Handheld Supernova", 10, 20, 5);
	wizard->displaySpell(fireball);


	cout << "=========================================" << endl;
	cout << "VERSus" << endl;
	cout << "=========================================" << endl;

	Wizard* target = new Wizard("Invoker", 100, 10);
	Spell* cast = new Spell(wizard, "Sun Strike", 10, 20, 5);
	wizard->getSpell()->cast(target);

	delete wizard;

	return 0;
}