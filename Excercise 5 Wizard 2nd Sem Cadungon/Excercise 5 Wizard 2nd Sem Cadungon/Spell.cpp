#include "pch.h"
#include "Spell.h"
#include "Wizard.h"
#include <time.h>



Spell::Spell(Wizard * actor, string name, int minDamage, int maxDamage, int mpCost)
{
	mActor = actor;
	mName = name;
	mDamageMin = minDamage;
	mDamageMax = maxDamage;
	mMpCost = mpCost;

	cout << "SpellType: " << mName << endl;
	cout << "Damage: " << mDamageMin << " - " << mDamageMax << endl;
	cout << "ManaCost: " << mMpCost << endl;

}

Spell::~Spell()
{
}

string Spell::getName()
{
	return mName;
}

int Spell::getDamageMin()
{
	return mDamageMin;
}

int Spell::getDamageMax()
{
	return mDamageMax;
}

int Spell::getMpCost()
{
	return mMpCost;
}

Wizard * Spell::getActor()
{
	return mActor;
}

void Spell::cast(Wizard * target)
{
	if (target == nullptr) return;

	if (mActor->getWillPower() < mMpCost) return;

	int wandDamage = rand() % (mDamageMax - mDamageMin + 1) + mDamageMin;


	mActor->deductWillPower(mMpCost);

	target->deductHealth(wandDamage);
}