#pragma once
#include <string>
#include <iostream>

class Wizard;

using namespace std;

class Spell
{
public:
	Spell(Wizard* actor, string name, int minDamage, int maxDamage, int mpCost);
	~Spell();

	string getName();
	int getDamageMin();
	int getDamageMax();
	int getMpCost();
	Wizard* getActor();

	void cast(Wizard* target);

private:
	string mName;
	int mDamageMin;
	int mDamageMax;
	int mMpCost;
	Wizard* mActor;

};
