#include "pch.h"
#include "Wizard.h"





Wizard::Wizard(string name, int health, int willPower)
{
	mName = name;
	mHealth = health;
	mWillpower = willPower;

	cout << "Wizard Name: " << mName << endl;
	cout << "HP: " << mHealth << endl;
	cout << "WillPower: " << mWillpower << endl;
}

Wizard::~Wizard()
{
}

string Wizard::getName()
{
	return mName;
}

void Wizard::displayName(string name)
{
	mName = name;
}

int Wizard::getHealth()
{
	return mHealth;
}

void Wizard::deductHealth(int damage)
{
	if (damage < 1)return;

	mHealth -= damage;
	if (mHealth < 0)
	{
		mHealth = 0;
	}
}

int Wizard::getWillPower()
{
	return mWillpower;
}

void Wizard::deductWillPower(int willPower)
{
	if (willPower < 0)return;
	if (mWillpower < willPower)return;

	mWillpower -= willPower;
}

Spell * Wizard::getSpell()
{
	return mSpell;
}

void Wizard::displaySpell(Spell * spell)
{
	if (mSpell != nullptr)delete mSpell;
	mSpell = spell;

}