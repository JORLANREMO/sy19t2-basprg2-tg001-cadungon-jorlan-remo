#pragma once
#include <string>
#include <iostream>


class Spell;
using namespace std;
class Wizard
{
public:
	Wizard(string name, int health, int willPower);
	~Wizard();

	string getName();
	void displayName(string name);

	int getHealth();
	void deductHealth(int damage);

	int getWillPower();
	void deductWillPower(int willPower);

	Spell* getSpell();
	void displaySpell(Spell * spell);

private:
	string mName;
	int mHealth;
	int mWillpower;
	Spell* mSpell;

};
