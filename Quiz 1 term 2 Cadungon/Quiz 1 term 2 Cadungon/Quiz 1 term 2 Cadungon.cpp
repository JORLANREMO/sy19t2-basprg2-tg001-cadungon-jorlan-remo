// Quiz 1 term 2 Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Sir I used reference in order for the code to work


//Wooden Armband
//15
//Leather Glove
//20
//Wool Hat
//25
//Focus Staff
//30
//Wooden Shield
//35
//Longsword
//120
//Crafter�s Knife
//140
//Longbow
//140
//Willpower Ring
//600
//Knight�s Helm
//900

//
//NPC Names
//Man
//Old Man
//Woman
//Girl
#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <time.h>
using namespace std;

struct Item
{
	string name;
	int gold;

};
struct NPC
{
	string npcName;
	Item itemOrder;
};

vector<Item> guildItems()
{

	vector<Item> guildItems = { {"WoodenArmband", 15 }, {"Leather Glove",20},{ "Wool Hat",25} ,{"Focus Staff",30},{ "Wooden Shield",35},{"Longsword",120},{ "Crafter�s Knife",140},{"Longbow",140},{ "Willpower Ring",600},{ "Knights Helm",900} };

	return guildItems;
}

Item sellItems(vector<Item> inventory)
{
	int r = rand() % inventory.size();
	return inventory[r];
}

NPC generateRandomNPC()
{
	int r = rand() % 4;
	NPC buyers[] = { { "Man"},{"Old Man"},{"Woman"},{"Girl"} };
	

	return buyers[r];
}

void printItems(vector<Item> items)
{
	int numberItems = items.size();

	for (int i = 0; i < numberItems; i++)
	{
		cout << items[i].name << " || " << items[i].gold << endl;
	}
	cout << endl;
	cout << "press (0) to go to store" << endl;
}


void printNPC(NPC npc, vector<Item> item, vector<Item>& inventory, int& gold)
{
	int random = rand() % item.size();
	bool bought;
	
		bought = false;
		cout << npc.npcName << " is looking at your item." << item[random].name << endl;
		for (int k = 0; k < inventory.size(); k++) 
		{
			if (item[random].name == inventory[k].name) 
			{
				cout << "You have that item" << endl;
				gold += inventory[k].gold;
				inventory.erase(inventory.begin() + k);
				bought = true;
				break;
			}
			
		}
		if (bought == false)
		{
			cout << "Sorry you don't have that item" << endl;
		}




}
void beginshop(vector<Item>item, vector<Item>& inventory, int & gold)
{
	system("cls");
	cout << "Begin to open your Store:" << endl;
	cout << "current gold: " << gold << endl;
	for (int i =0;i <5; i++)
	printNPC(generateRandomNPC(), item, inventory, gold);


	
}

void getItem(vector<Item> items, int& input, vector<Item>& inventory, int& gold) 
{
	Item item;
	bool isAccessible;
	do 
	{
		// the boolean will only accept the input within the range of 0 to 10;
		cin >> input;
		 isAccessible = input >= 0 && input < 11;
	} while(isAccessible == false);

	// when the input click is  0 the function beginshop will run.
	if (input == 0) 
	{
		beginshop(items, inventory,gold);
		system("pause");
	}

	else { 
		if (gold >= items[input - 1].gold) 
		{
			// player gold will minus the items you buy and the value for selling will increase its price.
			gold -= items[input - 1].gold;
			items[input - 1].gold += items[input - 1].gold * 0.5;
			inventory.push_back(items[input - 1]);
		}
	}
	

}

void toBuy(int& input, vector<Item> & inventory, int& gold)
{
	vector<Item> gItems = guildItems(); 
	printItems(gItems);

	getItem(gItems, input, inventory, gold);
}



int main()
{
	srand(time(NULL));

	int playerGold = 1000;
	int winGold = 10000;

	vector<Item> inventory;
	int input;
	while (playerGold >= 15) {
		
		cout << "Player's Gold:" << playerGold << endl;
		cout << "Inventory Items: " << endl;
		printItems(inventory);
		cout << endl << endl;

		toBuy(input, inventory, playerGold);

		if (playerGold >= winGold) 
		{
			cout << "Victory" << endl;
		}
			
		system("cls");
	}
}

