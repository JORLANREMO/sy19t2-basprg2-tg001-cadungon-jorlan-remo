// Quiz 2 the wall 2nd sem cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// You may want to use a variable to keep track
	// of the size, coz "<4" will not always be true

	//cout << "========= LOOP VERSION ==========" << endl;


	//Node* current = node1;
	//
	//for(int i = 0; i < 20; i++)// round
	//{
	//	cout << "round: " << i << endl;
	//	
	//	for (int i = 0; i < 4; i++) // soldiers
	//	{
	//	cout << current->name << endl;
	//	current = current->next;
	//	}
	//	system("pause");
	//	system("cls");

	//}
	//system("pause");



	//// Loop back last node to first node
	//// To make it circular
	// Before deleting a node, make sure to connect the 
	// next pointer
	//node2->next = node4;
	//delete node3;

//You must implement the group of Night�s Watch using a circular linked list.It is almost similar to the linked list.The only difference is that there is no NULL tail / link.The tail rotates back to the first node.
//
//Circular linked list doesn�t really have a starting point, unlike the usual linked list.In a circular linked list, any node can be the HEAD.You can tell that you�ve iterated over all the nodes if the tail points back to the head.
//
//You are required to use the provided struct (Node).This struct must be created dynamically using the new operator. Since this is a linked list, you are not allowed to use the native array or any collection object from the C++ standard library.Remember to deallocate these objects whenever necessary.
//

//You need to import this to your project and use this line in you main cpp file to use the struct :
//#include �Node.h�
//
//	Write functions whenever necessary.For every distinct actions you need to write a new function.
//	Some Readability tips :
//Relevant variable and function names
//Indent your code properly and consistently
//Don�t leave big whitespaces
//Add comments to every code block
//
//Sample Output
//What's your name soldier? Alliser
//What's your name soldier? Janos
//What's your name soldier? Othell
//What's your name soldier? Sam
//What's your name soldier? Snow
//
//==========================================
//ROUND 1
//==========================================
//Remaining Members :
//Alliser
//Janos
//Othell
//Sam
//Snow
//
//Result :
//Alliser has drawn 2
//Othell was eliminated
//
//==========================================
//ROUND 2
//==========================================
//Remaining Members :
//Sam
//Snow
//Alliser
//Janos
//
//Result :
//Sam has drawn 3
//Janos was eliminated
//
//==========================================
//ROUND 3
//==========================================
//Remaining Members :
//Sam
//Snow
//Alliser
//
//Result :
//Sam has drawn 3
//Sam was eliminated
//
//==========================================
//ROUND 4
//==========================================
//Remaining Members :
//Snow
//Alliser
//
//Result :
//Snow has drawn 1
//Alliser was eliminated
//
//==========================================
//FINAL RESULT
//==========================================
//Snow will go to seek for reinforcements.
//
//Press any key to continue . . .

#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <conio.h>

using namespace std;
struct Node
{
	string name;
	Node* next;
};

Node* randomizer(Node* head, int &x)
{
	Node* current = head;
	int random = rand() % x + 1;

	cout << head->name << " is Picking: " << random << endl;

	for (int i = 0; i < random - 1; i++)
	{
		current = current->next;

	}
	Node *del = current->next;
	current->next = current->next->next;
	cout << del->name << endl;
	current = current->next;


	delete del;
	x--;
	return current;
}

Node* displayAndRound(Node* head, int &x) 
{
	Node* current = head;
	
	cout << "Remaining troops: " << endl; // display all troops before the round has start
	for (int i = 0; i < 5; i++)
	{
		cout << current->name << endl;
		current = current->next;

	}
	system("pause");
	system("cls");
for (int i = 1; i <= 4; i++)// round start on round 1 
		{
	
		cout << "Round " << i << endl;
			/*for (int k = 0; k< 5; k++) // will go to the next link
			{
				cout << current->name << endl;
				current = current->next;
				
			}*/

		while (current->next != head) {
			cout << current->name << endl;
			current = current->next;
		}
		cout << current->name << endl;
		current = current->next;
			cout << endl;
			head = randomizer(head,x);
			current = head;
			system("pause");
			system("cls");
		}
return head;
		
}

int main()
{
	srand(time(NULL));
	int x = 5;
	Node* winner;
	Node* node1 = new Node;
	node1->name = "Jolo";

	Node* node2 = new Node;
	node2->name = "Yorme";
	node1->next = node2;

	Node* node3 = new Node;
	node3->name = "Isko";
	node2->next = node3;

	Node* node4 = new Node;
	node4->name = "Moreno";
	node3->next = node4;

	Node* node5 = new Node;
	node5->name = "Remo";
	node4->next = node5;

	node5->next = node1;
	
	/*node2->next = node4;
	delete node3;*/

	winner = displayAndRound(node1, x);
	

	
	cout << "Final Result: " << endl;
	cout<< winner->name << endl;
	system("pause");
	system("cls");

	return 0;
}