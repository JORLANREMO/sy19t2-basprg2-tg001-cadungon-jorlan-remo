// excercise 1 2nd sem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <time.h>
#include <conio.h>
#include <string>

using namespace std;

struct item 
{
	string name;
	int value;
};

int getRandomnumber() 
{
	int number = rand() % 10 + 1;
	return number;
}

item getItemlist() 
{
	string inventory[20] =
	{
		"last word",
		"hate",
		"dread",
		"infinity",
		"excalibur",
		"stradavar",
		"war",
		"grakatas",
		"lex",
		"trust",
		"truth",
		"broken war",
		"galatine",
		"lost vorpal",
		"inoruishi",
		"goldspeed",
		"manra of hope",
		"benzhon tower",
		"vectis",
		"lanka"
	};

	string randomName = inventory[rand() % 20];
	int moneyValue = rand() % 10 + 1;

	item randomItem = { randomName, moneyValue };

	cout << "item name: " << randomName << endl;
	cout << "money value: " << moneyValue << endl;
	cout << "" << endl;
	return randomItem;
}



int main()
{
	srand(time(NULL));

	string nameItem;


	item itemlist[10];
	for (int i = 0; i < 10; i++) 
	{
		itemlist[i] = getItemlist();
		
		system("pause");

	}
	_getch();
	return 0;
}


