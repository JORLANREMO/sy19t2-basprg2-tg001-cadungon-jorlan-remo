// excercise 3 2nd term Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//The player bets gold that is not zero or greater than current gold.
//AI rolls the 2 dice.The sum of the 2 dice will be the value of his roll.
//Player rolls the 2 dice.If the player rolls a higher value, he wins his bet.
//If player rolls snake eyes(1 - 1), he receives thrice his bet.
//If both the player and the AI rolls the same value, it�s a draw.This is also applicable with Snake Eyes.



#include "pch.h"
#include <iostream>
#include "time.h"
#include <string>
#include <conio.h>
using namespace std;

struct Dice 
{
	int diceA;
	int diceB;
};

void playerBet(int& input, int& gold, Dice& player, Dice& bot, int& totalPlayer, int& totalBot)
{
	player.diceA = rand() % 6 + 1;
	player.diceB = rand() % 6 + 1;
	bot.diceA = rand() % 6 + 1;
	bot.diceB = rand() % 6 + 1;
	
	// this is for testing
	//player.diceA = 1;
	//player.diceB = 1;
	//bot.diceA = 1;
	//bot.diceB = 1;

	cout << "Enter your bet: ";
	cin >> input;
	totalPlayer = player.diceA + player.diceB;
	totalBot = bot.diceA + bot.diceB;
	cout << "Player Roll: " << player.diceA << " | " << player.diceB << " = " << totalPlayer << endl;
	cout << "Bot Roll: " << bot.diceA << " | " << bot.diceB << " = " << totalBot << endl;
}

void compareDice(int& input, int& gold, Dice& player, Dice& bot, int& totalPlayer, int& totalBot)
{

	if ((totalPlayer > totalBot) && (totalBot != 2) )
	{
		cout << "You Win... " << endl;
		gold += input;
	}

	else if ((totalPlayer == 2) && (totalBot != 2) ) 
	{
		cout << " Snake Eyes You Win..." << endl;
		gold += input * 3;
	}

	else if (totalPlayer == totalBot)
	{
		cout << "Draw..." << endl;
	}

	else if (totalBot == 2) 
	{
		cout << " Snake Eyes You Lose..." << endl;
		gold -= input * 3;
	}
	

	else
	{
		cout << "You Lose..." << endl;
		gold -= input;
	}
}





int main()
{
	srand(time(NULL));
	//The player starts with 1000 gold.
	int playerGold = 1000;
	int input;
	Dice player;
	Dice bot;
	int Ptotal;
	int Btotal;
	//The game will only end if the player loses all his gold.
	while (playerGold > 0) 
	{
		cout << "Gold: " << playerGold << endl;
		
		
		

		playerBet(input, playerGold, player, bot,Ptotal,Btotal);
		compareDice(input, playerGold, player, bot,Ptotal, Btotal);
		//playerGold -= input;
		_getch();
		system("cls");
		if(playerGold <= 0)
		{ 
			cout << "You broke: " << playerGold << endl;
		}
	}



}


